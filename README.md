# Homework_2022

Это Шаблон проекта для ваших домашних работ курса fintech middle java 2022.

- Форкните этот проект себе. Создавать MR в этот репозиторий не нужно.
- Добавьте в maintainers вашего проекта преподавателей из раскрывающегося списка ниже, чтобы они могли проверять ваши домашние работы
- Сделайте свой проект приватным
- Приступайте к выполнению домашних работ
- Домашняя работа должна быть сделана в отдельной ветке вашего проекта, отведенной от основной ветки (main)
- Для сдачи работы необходимо оформить MR из ветки с выполненным заданием в основную ветку вашего проекта

<details><summary>Нажмите, чтобы увидеть список преподавателей</summary>

| Список преподавателей |
| ------ |
| a.yashnikov |
| vsevolodk |
| al.s.kashin |
| m.krasnozhen@tinkoff.ru |
| m.m.kharkov |
| alexyakovlev90 |
| Ulya0302 |
| p.trukan |
| zimin-krsk |
| xoma17 |
| Sin-Denis |
| alexey.pobedyonny |
| kadukm |
| vladwild220 |
| e.a.golova |
| edrachev |
| daria-kay |
| f1sherox |
| pashkinmv |
| eastygh |
</details>

## Как обновить форкнутый репозиторий
В примере адреса на репозиторий при подключении по SSH\
Если подключаетесь по HTTPS - поменяйте адрес на соответствующий
1. Добавить ссылку на основной репозиторий в проект
```
    git remote add tfs git@gitlab.com:tfs-java-middle/homework_2022.git
```
2. Убедиться, что основной репозиторий подключен
```
    git remote -v
```
```
origin  git@gitlab.com:your-login/homework_2022.git (fetch)
origin  git@gitlab.com:your-login/homework_2022.git (push)
tfs     git@gitlab.com:tfs-java-middle/homework_2022.git (fetch)
tfs     git@gitlab.com:tfs-java-middle/homework_2022.git (push)
```
3. Обновить информацию из основного репозитория
```
    git fetch tfs
```
4. Подтянуть изменения из основного репозитория себе
```
    git rebase tfs/main
```
5. Удалить ссылку на основной репозиторий, если не нужен больше
```
    git remote rm tfs
```
